import React, { useState } from "react";
import classes from "./index.module.scss";
import Head from "next/head";
import { featureList } from "../static-data/landingFeatureData";
import { cardData } from "../static-data/landingCardData";

import HeaderSubtext from "../components/Main/HeaderSubtext";
import SignupBttn from "../components/UI/SignupBttn";
import SignupBttn1 from "../components/UI/SignupBttn1";
import Companies from "../components/Main/Companies";
import Banner from "../components/Main/Banner";
import BannerImg from "../components/Main/BannerImg";
import ListWithImg from "../components/Main/ListWithImg";
import CardsGrid from "../components/Main/CardsGrid";
import Zero from "../components/Main/Zero";
import HeaderSubtextImg from "../components/Main/HeaderSubtextImg";

import Layout from "../components/Layout/Layout";
import SectionLayout from "../components/Layout/SectionLayout";

const LandingPage = () => {
  const deliveryFeatures = useState(featureList.deliveryData)[0];
  const strategyFeatures = useState(featureList.strategyData)[0];
  const frameworks = useState(featureList.frameworks)[0];
  const cardData1 = useState(cardData.cardData1)[0];
  const cardData2 = useState(cardData.cardData2)[0];

  return (
    <div className={classes.Landing} data-testid="landing">
      <Head>
        <title>Prodgoal — 100% Free Online Agile Product Management Tool</title>
        <meta
          name="description"
          content="Online Agile software development tool that makes productivity easy. Skyrocket your results while cutting costs. Unlimited users · Free forever · Cancel anytime"
        />
        <link rel="canonical" href="https://prodgoal.com" />
        <link rel="icon" type="image/x-icon" href="/images/favicon.ico" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0"
        ></meta>
      </Head>
      <Layout landing={true}>
        <main>
          <SectionLayout>
            <HeaderSubtext
              headline={
                <React.Fragment>
                  Free <span>Agile</span> &#38; <span>Kanban</span> Project
                  Management Tool
                </React.Fragment>
              }
              subtext={
                <React.Fragment>
                  100% free online Agile &#38; Kanban product and project
                  management tool
                  <br />
                  <span>
                    Unlimited users &#183; Free forever &#183; Cancel anytime
                  </span>
                </React.Fragment>
              }
              fontSizeHeadline="4.8rem"
              fontSizeSubtext="2rem"
              marginTop="154px"
            />
            <SignupBttn
              text="Get Started - It's Free!"
              fontSize="2.4rem"
              padding="1.6rem 2.4rem"
              marginTop="32px"
            />
            <Companies marginTop="36px" />

            <div className={classes.Video} style={{ marginTop: "28px" }}>
              <video
                src={
                  "/images/AutomaticForecasting/Free-Monte-Carlo-Simulation-Agile-Kanban-Forecasting.mp4"
                }
                width="886"
                autoPlay={true}
                controls={false}
                loop={true}
                muted={true}
              />
            </div>
          </SectionLayout>
          <Banner
            headline={<React.Fragment>Delivery features</React.Fragment>}
            subtext={
              <React.Fragment>
                Create boards and workflows, analyze each card with just a
                click,
                <br />
                use the backlog to plan and prioritize. Leverage insights and
                forecast automatically
              </React.Fragment>
            }
            marginTop="80px"
          />
          <SectionLayout>
            <ListWithImg elements={deliveryFeatures} marginTop="120px" />
          </SectionLayout>

          <Banner
            headline={<React.Fragment>Strategy features</React.Fragment>}
            subtext={
              <React.Fragment>
                Use canvases and next-gen 3.0 Roadmap to define the right goals,
                high-level workflow, epics,
                <br />
                their owners, and connect them to work items to track the
                progress and flow with advanced insights
              </React.Fragment>
            }
            marginTop="80px"
          />

          <SectionLayout>
            <ListWithImg elements={strategyFeatures} marginTop="120px" />
          </SectionLayout>

          <SignupBttn1 marginTop="160px" />

          <CardsGrid
            headline="Why manage projects with ProdGoal?"
            cards={cardData1}
            marginTop="160px"
          />
          <Zero marginTop="120px" />

          <CardsGrid
            headline="Who can benefit from ProdGoal?"
            cards={cardData2}
            marginTop="180px"
          />

          <SectionLayout>
            <HeaderSubtextImg
              headline="Improve your productivity like a pro"
              subtext={
                <React.Fragment>
                  Built on over 10+ years of professional experience,
                  <br />
                  ProdGoal is here to help you be productive in each step you
                  take.
                </React.Fragment>
              }
              marginTop="200px"
            />
          </SectionLayout>
          <Banner
            headline="Works with any framework"
            subtext={
              <React.Fragment>
                We are not limiting you to work in any given way. We are here to
                support you on your journey
                <br />
                and to enable you to succeed in any way that you prefer, be it
                Agile, Kanban, Scrum or your custom framework.
              </React.Fragment>
            }
            marginTop="200px"
          />

          <SectionLayout>
            <ListWithImg elements={frameworks} />
          </SectionLayout>

          <BannerImg marginTop="200px" headline="You are safe with us" />

          <SectionLayout>
            <HeaderSubtext
              headline={
                <React.Fragment>
                  Start managing projects with ProdGoal
                </React.Fragment>
              }
              subtext={
                <React.Fragment>
                  Unlimited users &#183; Free forever &#183; Cancel anytime
                </React.Fragment>
              }
              fontSizeHeadline="3.4rem"
              fontSizeSubtext="2rem"
              marginTop="200px"
            />
            <SignupBttn
              text="Get Started - It's Free!"
              fontSize="1.6rem"
              padding="1rem 2rem"
              marginTop="32px"
            />
          </SectionLayout>
        </main>
      </Layout>
    </div>
  );
};

export default LandingPage;
