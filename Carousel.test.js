import "@testing-library/jest-dom";
import { render, screen, within, act, fireEvent } from "@testing-library/react";

import Carousel from "../components/Main/Carousel";

describe("Carousel", () => {
  beforeEach(() => {
    jest.useFakeTimers();

    render(<Carousel />);
  });
  afterEach(() => {
    jest.useRealTimers();
  });

  it("renders properly", () => {
    expect(screen.getByTestId("carousel")).toBeInTheDocument();
  });

  it("useState textIdx maches the right img src from static carousel data", () => {
    let mockSrc =
      "http://localhost/_next/image?url=%2Fimages%2FCarousel%2FMultiLevelOverview.png&w=1920&q=75";
    let mockBody =
      "With just a click, put the OKR Roadmap next to your Backlog, or compare Product Vision with the team’s Board. Give everyone easy access to all Flight levels of your product";
    let mockHeadline = "Multi-Level Overview";

    let paragraph1 = screen.getByTestId("paragraph1");
    let imgElement = screen.getByTestId("image");
    let headingElement = within(paragraph1).getByRole("heading", { level: 4 });
    let bodyElement = within(paragraph1).getByTestId("body");
    let progress = within(paragraph1).getByTestId("progress");

    expect(imgElement.src).toEqual(mockSrc);
    expect(bodyElement.innerHTML).toEqual(mockBody);
    expect(headingElement.innerHTML).toEqual(mockHeadline);
    expect(progress).toHaveStyle("width: 1%");
  });

  it("setInterval increases progress on p1, then after 100 it disaperas, and progree on p1 shows with 1% w", () => {
    let paragraph1 = screen.getByTestId("paragraph1");
    let paragraph2 = screen.getByTestId("paragraph2");
    let progress1 = within(paragraph1).getByTestId("progress");

    act(() => {
      jest.advanceTimersByTime(50);
    });

    expect(progress1).toHaveStyle("width: 2%");

    act(() => {
      jest.advanceTimersByTime(50 * 98);
    });

    expect(progress1).toHaveStyle("width: 100%");

    act(() => {
      jest.advanceTimersByTime(50);
    });
    let progress2 = within(paragraph2).getByTestId("progress");

    expect(progress1).not.toBeInTheDocument();
    expect(progress2).toBeInTheDocument();
    expect(progress2).toHaveStyle("width: 1%");

    act(() => {
      jest.advanceTimersByTime(50);
    });
    expect(progress2).toHaveStyle("width: 2%");
  });

  it("click on paragraph makes progress 0% and shows proper image and text", () => {
    let mockSrc1 =
      "http://localhost/_next/image?url=%2Fimages%2FCarousel%2FMultiLevelOverview.png&w=1920&q=75";
    let mockSrc2 =
      "http://localhost/_next/image?url=%2Fimages%2FCarousel%2FAutomaticForecasting.png&w=1920&q=75";
    let mockBody1 =
      "With just a click, put the OKR Roadmap next to your Backlog, or compare Product Vision with the team’s Board. Give everyone easy access to all Flight levels of your product";
    let mockBody2 =
      "When it will be done?! Once a dreaded question, now an automatic function of the system. Stop wasting time estimating - using historical data we continuously forecast for you";
    let mockHeadline1 = "Multi-Level Overview";
    let mockHeadline2 = "Agile Forecasting";

    let paragraph1 = screen.getByTestId("paragraph1");
    let paragraph2 = screen.getByTestId("paragraph2");
    let headingElement1 = within(paragraph1).getByRole("heading", { level: 4 });
    let headingElement2 = within(paragraph2).getByRole("heading", { level: 4 });
    let bodyElement1 = within(paragraph1).getByTestId("body");
    let bodyElement2 = within(paragraph2).getByTestId("body");
    let progress1 = within(paragraph1).getByTestId("progress");
    let imgElement = screen.getByTestId("image");

    fireEvent.click(paragraph1);

    expect(paragraph1).toHaveClass("ChosenParag");
    expect(paragraph2).toHaveClass("null");
    expect(imgElement.src).toEqual(mockSrc1);
    expect(bodyElement1.innerHTML).toEqual(mockBody1);
    expect(headingElement1.innerHTML).toEqual(mockHeadline1);
    expect(progress1).toHaveStyle("width: 0%");

    fireEvent.click(paragraph2);

    let progress2 = within(paragraph2).getByTestId("progress");

    expect(paragraph2).toHaveClass("ChosenParag");
    expect(paragraph1).toHaveClass("null");
    expect(imgElement.src).toEqual(mockSrc2);
    expect(bodyElement2.innerHTML).toEqual(mockBody2);
    expect(headingElement2.innerHTML).toEqual(mockHeadline2);
    expect(progress2).toHaveStyle("width: 0%");
  });
});
