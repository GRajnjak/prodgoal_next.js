import React, { useState, useEffect } from "react";
import classes from "./Carousel.module.scss";
import { carouselParagraphs } from "../../static-data/carouselParagraphs";
import Image from "next/image";

const Carousel = () => {
  const [textIdx, setTextIdx] = useState(0);
  const [progressWidth, setProgressWidth] = useState(1);

  useEffect(() => {
    let slideInterval = setInterval(() => {
      let currentWidth = progressWidth;
      let currentIdx = textIdx;
      if (currentWidth === 0) {
        clearInterval(slideInterval);
      }
      if (currentWidth < 100 && currentWidth !== 0) {
        setProgressWidth((currentWidth) => currentWidth + 1);
      }
      if (currentWidth >= 100) {
        setProgressWidth(1);
        setTextIdx((currentIdx) => currentIdx + 1);
      }
    }, 50);
    return () => {
      clearInterval(slideInterval);
    };
  }, [progressWidth]);

  const paragraphChooseHandler = (id) => {
    setTextIdx(id - 1);
    setProgressWidth(0);
  };
  const clickToBttn = (val) => {
      amplitude.getInstance().logEvent(val)
  };

  let paragIdThatChanges =
    carouselParagraphs[textIdx % carouselParagraphs.length].id;
  let img = carouselParagraphs[textIdx % carouselParagraphs.length].img;

  return (
    <section className={classes.Section} data-testid="carousel">
      <div className={classes.Img}>
        <Image
          data-testid="image"
          priority={true}
          title="Free-Online-Agile-Kanban-Tool-Features"
          src={`/images/Carousel/${img}.png`}
          alt={img}
          width={653}
          height={350}
        />
      </div>
      <div className={classes.Text}>
        <ul>
          {carouselParagraphs.map((parag) => (
            <li
              data-testid={"paragraph" + parag.id}
              key={parag.id}
              className={` ${
                paragIdThatChanges === parag.id ? classes.ChosenParag : null
              } `}
              onClick={() => {
                paragraphChooseHandler(parag.id);
                clickToBttn("CarouselClickToParag" + parag.id);
              }}
            >
              <h4>{parag.header}</h4>
              <p data-testid="body">{parag.body}</p>
              {paragIdThatChanges === parag.id && (
                <div className={classes.ProgressContainer}>
                  <div
                    data-testid="progress"
                    className={classes.Progress}
                    style={{ width: progressWidth + "%" }}
                  />
                </div>
              )}
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default Carousel;
